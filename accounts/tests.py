from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth import authenticate, login, logout

from .views import homepage, loginView

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.models import User
from django.urls import reverse
import time

# Unit Test
class AuthAppTest(TestCase):
    def test_auth_app_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_auth_app_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_auth_app_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)
    
    def test_can_access_login_with_no_auth_using_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_can_access_login_with_no_auth_using_login_view_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginView)

    def test_cannot_access_logout_with_no_auth(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 404)

# Functional Test
class AuthAppFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(AuthAppFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(AuthAppFunctionalTest, self).tearDown()

    def test_title_element_in_project(self):
        self.browser.get(self.live_server_url)

        title = self.browser.find_element_by_tag_name('h1')
        self.assertIn("Welcome to Our Website", title.text)
        time.sleep(3)
    
    def test_no_auth_can_access_login(self):
        self.browser.get(self.live_server_url + "/login/")

        title = self.browser.find_element_by_tag_name('h1')
        self.assertIn("Welcome Back!", title.text)
        time.sleep(3)

    def test_can_login(self):
        # Create user
        User.objects.create_user(username='test', email='test@test.test', password='test123')

        self.browser.get(self.live_server_url + "/login/")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('test')
        password.send_keys('test123')
        submit.click()
        time.sleep(3)

        self.assertIn("Welcomeback test", self.browser.page_source)
        time.sleep(3)
    
    def test_can_signup(self):
        self.browser.get(self.live_server_url + "/login/")

        title = self.browser.find_element_by_id('title_signup')
        username = self.browser.find_element_by_id('username_signup')
        email = self.browser.find_element_by_id('email_signup')
        password = self.browser.find_element_by_id('password_signup')
        submit = self.browser.find_element_by_id('submit_signup')

        title.click()
        username.send_keys('test')
        email.send_keys('test@test.test')
        password.send_keys('test123')
        submit.click()
        time.sleep(3)

        self.assertIn("Welcomeback test", self.browser.page_source)
        time.sleep(3)

    def test_auth_can_access_logout(self):
        # Create user
        User.objects.create_user(username='test', email='test@test.test', password='test123')

        self.browser.get(self.live_server_url + "/login/")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('test')
        password.send_keys('test123')
        submit.click()
        time.sleep(3)

        logout = self.browser.find_element_by_id('logout')
        logout.click()
        time.sleep(3)

        self.assertIn("Welcome to Our Website", self.browser.page_source)
        time.sleep(3)

    def test_login_with_wrong_username(self):
        self.browser.get(self.live_server_url + "/login/")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('test')
        password.send_keys('test123')
        submit.click()
        time.sleep(3)

        self.assertIn("Welcome Back!", self.browser.page_source)
        time.sleep(3)

    def test_signup_with_same_username(self):
        # Create user
        User.objects.create_user(username='test', email='test@test.test', password='test123')

        self.browser.get(self.live_server_url + "/login/")

        title = self.browser.find_element_by_id('title_signup')
        username = self.browser.find_element_by_id('username_signup')
        email = self.browser.find_element_by_id('email_signup')
        password = self.browser.find_element_by_id('password_signup')
        submit = self.browser.find_element_by_id('submit_signup')

        title.click()
        username.send_keys('test')
        email.send_keys('test@test.test')
        password.send_keys('test123')
        submit.click()
        time.sleep(3)

        self.assertIn("Welcome Back!", self.browser.page_source)
        time.sleep(3)
    
    def test_auth_cannot_access_login(self):
        # Create user
        User.objects.create_user(username='test', email='test@test.test', password='test123')

        self.browser.get(self.live_server_url + "/login/")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('test')
        password.send_keys('test123')
        submit.click()
        time.sleep(3)

        self.browser.get(self.live_server_url + "/login/")

        self.assertIn("Welcomeback test", self.browser.page_source)
        time.sleep(3)
