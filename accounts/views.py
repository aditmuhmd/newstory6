from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.

def homepage(request):
    context = { 
        'sapaan' : 'Welcome to Our Website',
        'please' : 'Please Log In or Sign Up First',
        'log' : 'login',
    }
    if request.user.is_authenticated:
        context['sapaan'] = request.session['sapaan']
        context['please'] = request.session['please']
        context['log'] = request.session['log']

    return render(request, 'homepage.html', context)

def loginView(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            return redirect('homepage')
        else:
            return render(request, 'login.html')

    if request.method == "POST":
        if len(request.POST) == 3:
            username_login = request.POST['username']
            password_login = request.POST['password']
                    
            user = authenticate(request, username=username_login, password=password_login)
            
            if user is not None:
                login(request, user)
                request.session['sapaan'] = 'Welcomeback ' + request.user.username
                request.session['please'] = 'Please Log Out'
                request.session['log'] = 'logout'
                return redirect('homepage')
            else:
                return redirect('login')
        elif len(request.POST) == 4:
            username_login = request.POST['username']
            email_login = request.POST['email']
            password_login = request.POST['password']

            users = User.objects.all()

            for user in users:
                if user.username == username_login :
                    return redirect('login')

            user = User.objects.create_user(username=username_login, email=email_login, password=password_login)
            login(request, user)
            request.session['sapaan'] = 'Welcomeback ' + request.user.username
            request.session['please'] = 'Please Log Out'
            request.session['log'] = 'logout'
            return redirect('homepage')

 
@login_required
def logoutView(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            logout(request)
            
    return redirect('homepage')
