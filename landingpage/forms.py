from django import forms
from django.forms import widgets
from .models import Status

class StatusForm(forms.ModelForm):

    class Meta:
        model = Status
        fields = ['status']
        widgets = {
        'status' : forms.TextInput(attrs={'required': True}),
        }

    def __init__(self, *args, **kwargs):
        super(StatusForm,self).__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
