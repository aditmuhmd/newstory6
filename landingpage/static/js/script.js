var checkbox = document.querySelector('#switch');

$(".switcher").change(function () {
    if(this.checked) {
        trans()
        document.documentElement.setAttribute('data-theme', 'dark')
    } else {
        trans()
        document.documentElement.setAttribute('data-theme', 'light')
    }
});

let trans = () => {
    document.documentElement.classList.add('transition');
    window.setTimeout(() => {
        document.documentElement.classList.remove('transition')
    }, 1000)
}

jQuery(document).ready(function($){
    // Hide all panels to start
    var panels = $('.accordion > dd').hide();

    // On click of panel title
    $('.accordion > dt > a').click(function() {
        var $this = $(this)

        // SLide up all other panels
        panels.slideUp();

        // Slide down target panel
        $this.parent().next().slideDown();

        return false;
    });
});