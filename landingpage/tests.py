from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.http import HttpRequest
from django.apps import apps
import time

from .apps import LandingpageConfig
from .views import index
from .models import Status
from .forms import StatusForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class Story6UnitTest(TestCase):

    def test_landingpage_url_is_exist(self):
        response = Client().get('/landingpage/')
        self.assertEqual(response.status_code, 200)
    
    def test_landingpage_using_index_func(self):
        found = resolve('/landingpage/')
        self.assertEqual(found.func, index)

    def test_story6_using_index_template(self):
        response = Client().get('/landingpage/')
        self.assertTemplateUsed(response, 'index.html')

    def test_landingpage_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Welcome to my Website", html_response)

    def test_create_status(self):
        new_status = Status.objects.create(status = "Test Status")
        return new_status
    
    def test_check_status(self):
        check = self.test_create_status()
        self.assertTrue(isinstance(check, Status))
        self.assertTrue(check.__str__(), check.status)
        count_all_status = Status.objects.all().count()
        self.assertEqual(count_all_status, 1)
    
    def test_form_StatusForm(self):
        test_data = {
            'status' : 'statusku masih single :)',
        }
        form = StatusForm(data = test_data)
        self.assertTrue(form.is_valid())

        request = self.client.post('/landingpage/', data = test_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/landingpage/')
        self.assertEqual(response.status_code, 200)
    

class Story6FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
    
    def test_input_status_from_user(self):
        time.sleep(3)
        self.browser.get(self.live_server_url+"/landingpage/")
        time.sleep(3)
        
        # Test if HTML appear a sentences "Welcome to my Website"
        self.assertInHTML("Welcome to my Website", self.browser.page_source)

        # Test status and the submit button in Landing Page
        status = self.browser.find_element_by_id('id_status')
        submit = self.browser.find_element_by_id('submit')

        status.send_keys("Coba-coba")
        submit.send_keys(Keys.ENTER)

    def test_switch_theme_label(self):
        browser = self.browser
        time.sleep(3)
        browser.get(self.live_server_url+"/landingpage/")
        time.sleep(3)
        label = "switch"
        self.assertIn(label, browser.page_source)

    def test_functional_switch_label_theme_dark(self):
        browser = self.browser
        time.sleep(3)
        browser.get(self.live_server_url+"/landingpage/")
        time.sleep(3)
        label = browser.find_element_by_class_name("toggle-containter")
        label.click()
        background_web =  browser.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEquals('rgba(252, 252, 252, 1)', background_web)

    def test_functional_switch_label_theme_light(self):
        browser = self.browser
        time.sleep(3)
        browser.get(self.live_server_url+"/landingpage/")
        time.sleep(3)
        label = browser.find_element_by_class_name("toggle-containter")
        label.click()
        background_web =  browser.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEquals('rgba(252, 252, 252, 1)', background_web)

    def test_accordion(self):
        browser = self.browser
        time.sleep(3)
        browser.get(self.live_server_url+"/landingpage/")
        time.sleep(3)
        accordion = browser.find_element_by_class_name("accordion")
        self.assertIsNotNone(accordion)