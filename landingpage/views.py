from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status

def index(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('index')
    else:
        form = StatusForm()

    status = Status.objects.all().order_by('date')
    return render(request, 'index.html', {'form': form, 'Status': status})